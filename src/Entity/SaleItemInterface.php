<?php

namespace Kematjaya\SaleBundle\Entity;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;
use Kematjaya\SaleBundle\Entity\SaleInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface SaleItemInterface extends StoreStockCardTransactionInterface
{
    public function getItem(): ?ItemInterface;

    public function getSale(): ?SaleInterface;

    public function getQuantity(): ?float;

    public function getPrincipalPrice(): ?float;

    public function setPrincipalPrice(float $principal_price): self;

    public function getSalePrice(): ?float;

    public function getSubTotal(): ?float;

    public function getTax(): ?float;
    
    public function getDiscount(): ?float;
    
    public function getTotal(): ?float;

    public function setTotal(float $total): self;
}
