<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\SaleBundle\FormSubscriber;

use Kematjaya\SaleBundle\Entity\SaleItemInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *
 * @author apple
 */
interface SaleItemFormSubscriberInterface extends EventSubscriberInterface 
{
    const TAG_NAME = 'sale_item.form_subscriber';
    
    public function isSupport(SaleItemInterface $saleItem):bool;
}
