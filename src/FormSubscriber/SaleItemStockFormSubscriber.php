<?php

namespace Kematjaya\SaleBundle\FormSubscriber;

use Kematjaya\SaleBundle\Entity\SaleItemInterface;
use Kematjaya\ItemPackBundle\Entity\StockInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;

/**
 * Description of SaleItemStockFormSubscriber
 *
 * @author apple
 */
class SaleItemStockFormSubscriber implements SaleItemFormSubscriberInterface 
{
    //put your code here
    public function isSupport(SaleItemInterface $saleItem): bool 
    {
        return false;//!$saleItem instanceof SaleItemPackagingInterface;
    }

    public static function getSubscribedEvents():array
    {
        return [
            FormEvents::POST_SUBMIT => 'postSubmit'
        ];
    }
    
    public function postSubmit(FormEvent $event):void
    {
        $data = $event->getData();
        if (!$data instanceof SaleItemInterface) {
            return;
        }
        
        $item = $data->getItem();
        $form = $event->getForm();
        if (!$item instanceof StockInterface) {
            return;
        }
        
        if ($item->isCountStock() && $item->getLastStock() < $data->getQuantity()) {
            $form->get('quantity')->addError(
                new FormError(sprintf('not sufficient stock for product %s, available: %s', $item->getName(), $item->getLastStock()))
            );
            return;
        }
    }
}
