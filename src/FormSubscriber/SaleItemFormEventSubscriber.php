<?php

namespace Kematjaya\SaleBundle\FormSubscriber;

use Kematjaya\SaleBundle\Entity\SaleItemInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleItemFormEventSubscriber implements SaleItemFormSubscriberInterface
{
    public function isSupport(SaleItemInterface $saleItem):bool
    {
        return true;
    }
    
    public static function getSubscribedEvents():array
    {
        return [
            FormEvents::POST_SUBMIT => 'postSubmit'
        ];
    }
    
    public function postSubmit(FormEvent $event):void
    {
        $data = $event->getData();
        if (!$data instanceof SaleItemInterface) {
            return;
        }
        
        $item = $data->getItem();
        $form = $event->getForm();
        if ($item->getLastPrice() <= 0) {
            $form->get('sale_price')->addError(
                new FormError(sprintf('price not set for item %s', $item->getName()))
            );
            return;
        }

        if ($item->getLastPrice() <= $item->getPrincipalPrice()) {
            $form->get('sale_price')->addError(
                new FormError(sprintf('sale price (%s) less than principal price (%s), please contact administrator.', $item->getLastPrice(), $item->getPrincipalPrice()))
            );
            return;
        }
        
        $data->setPrincipalPrice(
            $data->getItem()->getPrincipalPrice()
        );
        
        $event->setData($data);
    }

}
