<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\SaleBundle\FormSubscriber;

use Kematjaya\SaleBundle\Entity\SaleInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *
 * @author programmer
 */
interface SaleFormSubscriberInterface extends EventSubscriberInterface 
{
    const TAG_NAME = 'sale.form_subscriber';
    
    public function isSupport(SaleInterface $sale):bool;
}
