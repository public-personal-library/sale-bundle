<?php

namespace Kematjaya\SaleBundle\FormSubscriber;

use Kematjaya\SaleBundle\Entity\SaleInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Description of SaleLockFormSubscriber
 *
 * @author programmer
 */
class SaleLockFormSubscriber implements SaleFormSubscriberInterface 
{
    
    private TokenStorageInterface $tokenStorage;
    
    public function __construct(TokenStorageInterface $tokenStorage) 
    {
        $this->tokenStorage = $tokenStorage;
    }
    
    public function isSupport(SaleInterface $sale): bool 
    {
        return true;
    }

    public static function getSubscribedEvents():array 
    {
        return [
            FormEvents::POST_SUBMIT => 'prepareData'
        ];
    }
    
    public function prepareData(FormEvent $event):void
    {
        $data = $event->getData();
        if (null === $this->tokenStorage->getToken()) {
            $event->getForm()
                    ->addError(
                        new FormError("anda harus login.")
                    );
            return;
        }
        
        $token = $this->tokenStorage->getToken();
        $userIdentifier = (method_exists($token, "getUserIdentifier")) ? $token->getUserIdentifier():$token->getUsername();
        $data->setCreatedBy(
            $userIdentifier
        );
        
        $event->setData($data);
    }

}
