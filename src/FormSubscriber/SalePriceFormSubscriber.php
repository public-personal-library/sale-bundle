<?php

namespace Kematjaya\SaleBundle\FormSubscriber;

use Kematjaya\SaleBundle\Entity\SaleInterface;
use Kematjaya\PriceBundle\Type\PriceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Description of SalePriceFormSubscriber
 *
 * @author programmer
 */
class SalePriceFormSubscriber implements SaleFormSubscriberInterface 
{
    public function isSupport(SaleInterface $sale): bool 
    {
        return null !== $sale->getCode();
    }

    public static function getSubscribedEvents():array 
    {
        return [
            FormEvents::POST_SET_DATA => 'setForm'
        ];
    }
    
    public function setForm(FormEvent $event):void
    {
        $event->getForm()
                ->add('sub_total', PriceType::class, [
                    'label' => 'sub_total',
                    'attr' => ['style' => 'text-align: right']
                ])
                ->add('tax', PriceType::class, [
                    'label' => 'tax',
                    'attr' => ['style' => 'text-align: right']
                ])
                ->add('discount', PriceType::class, [
                    'label' => 'discount', 
                    'attr' => ['style' => 'text-align: right']
                ]);
    }

}
