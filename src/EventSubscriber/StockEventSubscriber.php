<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\SaleBundle\EventSubscriber;

use Kematjaya\ItemPackBundle\Entity\ClientStockCardInterface;
use Kematjaya\ItemPackBundle\Manager\StockManagerInterface;
use Kematjaya\ItemPackBundle\Service\StockCardServiceInterface;
use Kematjaya\SaleBundle\Event\PostUpdateSaleItemEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Description of StockEventSubscriber
 *
 * @author programmer
 */
class StockEventSubscriber implements EventSubscriberInterface 
{
    private StockManagerInterface $stockManager;
    
    private StockCardServiceInterface $stockCardService;
    
    public function __construct(StockManagerInterface $stockManager, StockCardServiceInterface $stockCardService) 
    {
        $this->stockManager = $stockManager;
        $this->stockCardService = $stockCardService;
    }
    
    public static function getSubscribedEvents():array
    {
        return [
            PostUpdateSaleItemEvent::EVENT_NAME => 'updateStock'
        ];
    }

    public function updateStock(PostUpdateSaleItemEvent $event):void
    {
        $entity = $event->getEntity();
        $this->stockManager->getStock(
            $entity->getItem(), 
            $entity
        );
        
        if (!$entity instanceof ClientStockCardInterface) {
            return;
        }
        
        $this->stockCardService->insertStockCard(
            $entity->getItem(), 
            $entity
        );
    }
}
