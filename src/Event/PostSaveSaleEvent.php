<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\SaleBundle\Event;

use Kematjaya\SaleBundle\Entity\SaleInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Description of PostSaveSaleEvent
 *
 * @author programmer
 */
class PostSaveSaleEvent extends Event 
{
    /**
     * 
     * @var SaleInterface
     */
    private $entity;
    
    const EVENT_NAME = "sale.post_save_sale";
    
    public function __construct(SaleInterface $entity) 
    {
        $this->entity = $entity;
    }
    
    public function getEntity(): SaleInterface 
    {
        return $this->entity;
    }
}
