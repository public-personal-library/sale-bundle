<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\SaleBundle\Event;

use Kematjaya\SaleBundle\Entity\SaleItemInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Description of PostUpdateSaleItemEvent
 *
 * @author programmer
 */
class PostUpdateSaleItemEvent extends Event 
{
    /**
     * 
     * @var SaleItemInterface
     */
    private $entity;
    
    const EVENT_NAME = "sale.post_update_item_sale";
    
    public function __construct(SaleItemInterface $entity) 
    {
        $this->entity = $entity;
    }
    
    public function getEntity(): SaleItemInterface 
    {
        return $this->entity;
    }
}
