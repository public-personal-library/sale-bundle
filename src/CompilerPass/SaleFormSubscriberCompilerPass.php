<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\SaleBundle\CompilerPass;

use Kematjaya\SaleBundle\Builder\SaleFormSubscriberBuilderInterface;
use Kematjaya\SaleBundle\FormSubscriber\SaleFormSubscriberInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Description of SaleFormSubscriberCompilerPass
 *
 * @author programmer
 */
class SaleFormSubscriberCompilerPass implements CompilerPassInterface 
{
    public function process(ContainerBuilder $container) 
    {
        $definition = $container->findDefinition(SaleFormSubscriberBuilderInterface::class);
        $taggedServices = $container->findTaggedServiceIds(SaleFormSubscriberInterface::TAG_NAME);
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addSubscriber', [new Reference($id)]);
        }
    }
}
