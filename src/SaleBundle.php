<?php

namespace Kematjaya\SaleBundle;

use Kematjaya\SaleBundle\FormSubscriber\SaleItemFormSubscriberInterface;
use Kematjaya\SaleBundle\FormSubscriber\SaleFormSubscriberInterface;
use Kematjaya\SaleBundle\CompilerPass\SaleFormSubscriberCompilerPass;
use Kematjaya\SaleBundle\CompilerPass\SaleItemFormSubscriberCompilerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(SaleFormSubscriberInterface::class)
                ->addTag(SaleFormSubscriberInterface::TAG_NAME);
        $container->registerForAutoconfiguration(SaleItemFormSubscriberInterface::class)
                ->addTag(SaleItemFormSubscriberInterface::TAG_NAME);
        
        $container->addCompilerPass(new SaleFormSubscriberCompilerPass());
        $container->addCompilerPass(new SaleItemFormSubscriberCompilerPass());
        
        parent::build($container);
    }
}
