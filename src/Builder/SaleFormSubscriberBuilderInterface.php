<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\SaleBundle\Builder;

use Kematjaya\SaleBundle\FormSubscriber\SaleFormSubscriberInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 *
 * @author programmer
 */
interface SaleFormSubscriberBuilderInterface 
{
    public function addSubscriber(SaleFormSubscriberInterface $saleFormSubscriber):self;
    
    public function implementSubscriber(FormBuilderInterface $builder, array $options):void;
}
