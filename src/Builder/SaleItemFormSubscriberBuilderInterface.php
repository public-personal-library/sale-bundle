<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\SaleBundle\Builder;

use Kematjaya\SaleBundle\FormSubscriber\SaleItemFormSubscriberInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 *
 * @author apple
 */
interface SaleItemFormSubscriberBuilderInterface 
{
    public function addSubscriber(SaleItemFormSubscriberInterface $subscriber):self;
    
    public function implementSubscriber(FormBuilderInterface $builder, array $options):void;
}
