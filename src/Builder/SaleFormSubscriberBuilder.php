<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\SaleBundle\Builder;

use Kematjaya\SaleBundle\Entity\SaleInterface;
use Kematjaya\SaleBundle\FormSubscriber\SaleFormSubscriberInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of SaleFormSubscriberBuilder
 *
 * @author programmer
 */
class SaleFormSubscriberBuilder implements SaleFormSubscriberBuilderInterface 
{
 
    private ArrayCollection $subscriber;
    
    public function __construct() 
    {
        $this->subscriber = new ArrayCollection();
    }
    
    public function addSubscriber(SaleFormSubscriberInterface $saleFormSubscriber): SaleFormSubscriberBuilderInterface 
    {
        if (!$this->subscriber->contains($saleFormSubscriber)) {
            $this->subscriber->add($saleFormSubscriber);
        }
        
        return $this;
    }

    public function implementSubscriber(FormBuilderInterface $builder, array $options): void 
    {
        $object = $options["data"];
        if (!$object instanceof SaleInterface) {
            return;
        }
        
        $subscribers = $this->subscriber->filter(function (SaleFormSubscriberInterface $saleFormSubscriber) use ($object) {
            return $saleFormSubscriber->isSupport($object);
        });
        
        foreach ($subscribers as $subscriber) {
            $builder->addEventSubscriber($subscriber);
        }
    }

}
