<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\SaleBundle\Builder;

use Kematjaya\SaleBundle\Entity\SaleItemInterface;
use Kematjaya\SaleBundle\FormSubscriber\SaleItemFormSubscriberInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of SaleItemFormSubscriberBuilder
 *
 * @author apple
 */
class SaleItemFormSubscriberBuilder implements SaleItemFormSubscriberBuilderInterface 
{
    
    private ArrayCollection $subscriber;
    
    public function __construct() 
    {
        $this->subscriber = new ArrayCollection();
    }
    
    public function addSubscriber(SaleItemFormSubscriberInterface $subscriber):SaleItemFormSubscriberBuilderInterface
    {
        if (!$this->subscriber->contains($subscriber)) {
            $this->subscriber->add($subscriber);
        }
        
        return $this;
    }
    
    public function implementSubscriber(FormBuilderInterface $builder, array $options):void
    {
        $object = $options["data"];
        if (!$object instanceof SaleItemInterface) {
            return;
        }
        
        $subscribers = $this->subscriber->filter(function (SaleItemFormSubscriberInterface $subscriber) use ($object) {
            return $subscriber->isSupport($object);
        });
        
        foreach ($subscribers as $subscriber) {
            $builder->addEventSubscriber($subscriber);
        }
    }
}
