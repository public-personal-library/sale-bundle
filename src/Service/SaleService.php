<?php

namespace Kematjaya\SaleBundle\Service;

use Kematjaya\SaleBundle\Event\PostUpdateSaleItemEvent;
use Kematjaya\SaleBundle\Event\PostSaveSaleEvent;
use Kematjaya\SaleBundle\Entity\SaleInterface;
use Kematjaya\SaleBundle\Entity\SaleItemInterface;
use Kematjaya\SaleBundle\Repo\SaleRepoInterface;
use Kematjaya\SaleBundle\Repo\SaleItemRepoInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class SaleService implements SaleServiceInterface
{
    private SaleRepoInterface $saleRepository;
    
    private SaleItemRepoInterface $saleItemRepository;
    
    private EventDispatcherInterface $eventDispatcher;
    
    function __construct(EventDispatcherInterface $eventDispatcher, SaleRepoInterface $saleRepository, SaleItemRepoInterface $saleItemRepository) 
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->saleRepository = $saleRepository;
        $this->saleItemRepository = $saleItemRepository;
    }
    
    public function update(SaleInterface $entity): void
    {
        if (!$entity->getIsLocked()) {
            return;
        }
            
        $this->updatePaymentChange($entity);
        
        $this->saleRepository->save($entity);
        
        $this->eventDispatcher->dispatch(
            new PostSaveSaleEvent($entity),
            PostSaveSaleEvent::EVENT_NAME
        );
    }
    
    protected function countSubTotal(SaleInterface $entity):float
    {
        $subTotal = 0;
        foreach ($entity->getSaleItems() as $saleItem) {
            if (!$saleItem instanceof SaleItemInterface) {
                continue;
            }
            
            $itemSubTotal = $saleItem->getQuantity() * $saleItem->getSalePrice();
            $total = ($itemSubTotal + $saleItem->getTax()) - $saleItem->getDiscount();
            $saleItem->setTotal($total);
            $this->saleItemRepository->save($saleItem);
            
            $subTotal += $total;

            $this->eventDispatcher->dispatch(
                new PostUpdateSaleItemEvent($saleItem),
                PostUpdateSaleItemEvent::EVENT_NAME
            );
        }
        
        return $subTotal;
    }
    
    protected function updatePaymentChange(SaleInterface $entity): void
    {
        $subTotal = $this->countSubTotal($entity);
        
        $tax        = null !== $entity->getTax() ? $entity->getTax() : 0;
        $discount   = null !== $entity->getDiscount() ? $entity->getDiscount() : 0;
        $payment    = null !== $entity->getPayment() ? $entity->getPayment() : 0;
        $total      = ($subTotal + $tax) - $discount;
        $paymentChange = $payment - $total;
        
        $entity->setSubTotal($subTotal);
        $entity->setTotal($total);
        $entity->setPaymentChange($paymentChange);
    }
}
