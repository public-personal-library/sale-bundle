<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\SaleBundle\Tests;

use Kematjaya\SaleBundle\Entity\SaleItemInterface;
use Kematjaya\SaleBundle\Entity\SaleInterface;
use Kematjaya\SaleBundle\Repo\SaleItemRepoInterface;
use Kematjaya\SaleBundle\Repo\SaleRepoInterface;
use Kematjaya\SaleBundle\Service\SaleService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of SaleServiceTest
 *
 * @author programmer
 */
class SaleServiceTest extends \PHPUnit\Framework\TestCase
{
    public function testNotLocked()
    {
        $saleRepository = $this->createMock(SaleRepoInterface::class);
        $saleRepository->expects($this->never())
                ->method("save");
        
        $saleItemRepository = $this->createMock(SaleItemRepoInterface::class);
        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        
        $service = new SaleService(
            $eventDispatcher,
            $saleRepository, 
            $saleItemRepository
        );
        
        $entity = $this->createMock(SaleInterface::class);
        $entity->expects($this->once())
                ->method("getIsLocked")
                ->willReturn(false);
        
        $service->update($entity);
    }
    
    public function testLocked()
    {
        $saleRepository = $this->createMock(SaleRepoInterface::class);
        $saleRepository->expects($this->once())
                ->method("save");
        
        $saleItemRepository = $this->createMock(SaleItemRepoInterface::class);
        $saleItemRepository->expects($this->any())
                ->method("save");
        
        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects($this->any())
                ->method("dispatch");
        
        $service = new SaleService(
            $eventDispatcher,
            $saleRepository, 
            $saleItemRepository
        );
        
        $itemSale = $this->createMock(SaleItemInterface::class);
        $entity = $this->createMock(SaleInterface::class);
        $entity->expects($this->once())
                ->method("getIsLocked")
                ->willReturn(true);
        
        $entity->expects($this->once())
                ->method("getSaleItems")
                ->willReturn(
                    new ArrayCollection([
                        $itemSale
                    ])
                );
        
        $entity->expects($this->once())
                ->method("setSubTotal");
        $entity->expects($this->once())
                ->method("setTotal");
        $entity->expects($this->once())
                ->method("setPaymentChange");
        
        $service->update($entity);
    }
}
